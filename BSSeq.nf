#!/usr/bin/env nextflow 
// -*- mode: java -*-
/*
####################################################################
### BS-Seq Methylation Analysis : Trimming, Mapping and Cleaning ###
####################################################################
Script started in July 2018. 
Authors :
    Coralie Gimonnet <coralie.gimonnet@inra.fr>
    Benoît Piégu <benoit.piegu@inra.fr>
This pipeline was adapted for WGBS analysis with paired-end data.
Version 1.6
*/


/* Default parameters */

params.genome = "genome.fasta"
params.reads = "*_R{1,2}.fq.gz" 
// params.outdir ='results'
params.cov = 0

params.notrim = false
params.help = false
params.fastqc = false
params.index = false
params.se = false
params.extract = false
params.trim_galore_options = '' 
params.directional_bismark = false


read_pairs = file(params.reads)
fasta = file(params.genome)
 

def helpMessage() {
  log.error """\
        BISMARK ALIGNMENT PIPELINE
      ========================================
      default parameters :
        genome : ${params.genome}
        reads  : ${params.reads}
        outdir : ${params.outdir}
        cov : ${params.cov}
      optional parameters :
        index : path to bismark index files [default : FALSE]
        se : single-end data [default : paired-end] - se not optimized for the moment
        fastqc : make a FastQC analysis [default : FALSE]
        notrim : skip trimming step [default : FALSE]
        bedGraph : make a bedGraph of alignment - bedGraph creation not fonctionnal for the moment
        extract : extract methylation [default: FALSE]
        trim_galore_options :  trim_galore options excluding --cores, --paired and --gzip options
          example: --trim_galore_options "--stringency 5 --clip_R1 10 --clip_R2 10"
        directional_bismark : bismark run in directional mode [default: FALSE]
      Warning : This pipeline assumes all software are on the PATH.
      """
      .stripIndent()
}

// Show help message
if (params.help) {
    helpMessage()
    exit 0
}


 
/* 
* Create the `read_pairs` channel that emits tuples containing three elements :
* the pair ID, the first read-pair file and the second read-pair file
*/

Channel
  .fromFilePairs( params.reads, size: params.se ? 1 : 2 )
  .ifEmpty { exit 1, "Cannot find any reads matching: ${params.reads}" }
  .into { read_files_fastqc ; read_files_trimming ; reads_mapping } 


/*
* Step 1. Builds the genome index required by the mapping process
*/
if (params.index) {
    bismark_index = Channel
    .fromPath(params.index)
    .ifEmpty { exit 1, "Bismark index not found : ${params.index}" }
} else {
    process buildIndex {
        publishDir "${params.outdir}/reference_genome", mode: 'symlink'
        input:
          file "genome.fasta" from fasta
        output:
          file "BismarkIndex" into bismark_index
        script:
          """
          mkdir BismarkIndex
          cp ${fasta} BismarkIndex/
          bismark_genome_preparation --verbose BismarkIndex
          """
  }
}

 
/*
* Step 2. FASTQC Analysis
*/
if (params.fastqc) {
    process fastqc {
        tag "$pair_id"
        publishDir "${params.outdir}/fastqc", mode: 'symlink',
        saveAs: {filename -> filename.indexOf(".zip") > 0 ? "zips/$filename" : "$filename"}
        input:
	tuple val(pair_id), file(reads) from read_files_fastqc
        output:
          file '*_fastqc.{zip,html}' into fastqc_results
        script:
          """
          fastqc -q $reads
         """
  }
} else {
  fastqc_results = Channel.from(false)
}


/*
* Step 3. Trim Galore!
*/
if (params.notrim){
  trimmed_reads = read_files_trimming
  trimgalore_results = Channel.from(false)
} else {
  process trimming {
    cpus 8
    tag "$pair_id"
    publishDir "${params.outdir}/trim_galore",mode: 'symlink',
    saveAs: { filename ->
                if (filename.indexOf("trimming_report.txt") > 0) "logs/$filename"
            }
    input:
    tuple val(pair_id), file(reads) from read_files_trimming
    output:
    tuple val(pair_id), file('*.fq.gz') into trimmed_reads
      file "*trimming_report.txt" into trimgalore_results
    script:
      if (params.se) {
          """
          trim_galore --cores ${task.cpus} --gzip $reads ${params.trim_galore_options}
          """
      } else {
          """
          trim_galore --cores ${task.cpus} --paired --gzip $reads ${params.trim_galore_options}
          """
      }
  }
}  

/*
* Step 4. Mapping with Bismark
*/
process mapping {
  core = 5
  cpus = core * 2
  // with slurm, nextflow must be able to declare the number of cpus that will actually
  // be used (cpus) and not the number declared with the --parallel option (core)
  // cf doc about --parallel option in bismark:  "However, please note that a typical Bismark
  // run will use several cores already (Bismark itself, 2 or 4 threads of Bowtie/Bowtie2,
  // Samtools, gzip etc...)"
  directional_option = params.directional_bismark ? '' : '--non_directional'
  tag "$pair_id"
  publishDir "${params.outdir}/mapping", mode: 'symlink',
    saveAs: {filename ->
      if (filename.indexOf("report.txt") > 0) "reports/$filename"
    }
  input:
    file index from bismark_index.collect()
    tuple val(pair_id), file(reads) from trimmed_reads
  output:
    file "*.bam" into bam_aln
    file "*report.txt" into bismark_align_log1, bismark_align_log2
    file "*.nucleotide_stats.txt" into bismark_nuc_log1
  script:
    if (params.se){
        """
        bismark --fastq --genome $index $reads -p ${task.core} ${task.directional_option} --nucleotide_coverage
        """
    } else {
        """
        bismark --fastq --genome $index -1 ${reads[0]} -2 ${reads[1]} --parallel ${task.core} ${task.directional_option} --nucleotide_coverage

        """
    }
}

/*
* Step 5. deduplicating with Bismark
*/
process bismark_deduplicate {
  cpus 10
  tag "${bam.simpleName}"
  paired_option = params.se ? '--single' : '--paired'
  publishDir "${params.outdir}/bismark_dedup", mode: 'symlink'
  input:
    file bam from bam_aln
  output:
    file "${bam.simpleName}.deduplicated.bam" into bam_bis_dedup, bam_bis_dedup_2_extract
    tuple file("${bam.simpleName}.deduplicated.log"), file("${bam.simpleName}*.deduplication_report.txt") into bismark_deduplicate_logs1, bismark_deduplicate_logs2
  script:
      """
      deduplicate_bismark ${task.paired_option} $bam -o ${bam.simpleName} >| ${bam.simpleName}.deduplicated.log 2>&1
      """
}


/*
* Step 6. Sorting alignment and stats
*/
process sorting {
  cpus 10
  tag "${bam.simpleName}"
  publishDir "${params.outdir}/sort", mode: 'symlink',
    saveAs: { filename -> 
              if (filename.indexOf(".txt") > 0) "logs/$filename" 
      }
  input:
    file bam from bam_bis_dedup
  output:
    file "${bam.simpleName}.sorted.bam" into bam_sorted, index_input, flagstat_input, bam_qualimap_input
    //file "${bam.simpleName}_flagstat.txt" into flagstat_input
    //file "${bam.simpleName}_flagstat.txt" into index_input
  script:
  """
    samtools sort $bam -@ ${task.cpus} -o ${bam.simpleName}.sorted.bam
  """
}

/*
* samtools index
*/
process index {
  cpus 1
  tag "${bam.simpleName}"
  publishDir "${params.outdir}/sort", mode: 'symlink'
  input :
    file bam from index_input
  output :
    file "${bam.simpleName}.sorted.bam.bai" into bam_index
  shell:
  """
    samtools index $bam
  """
}

/*
* Step 7. Flagstat of final bam
*/
process flagstat {
  cpus 1
  tag "${bam.simpleName}"
  publishDir "${params.outdir}/sort", mode: 'symlink'
  input :
    file bam from flagstat_input
    file bam_index from bam_index
  output:
    file "${bam.simpleName}.sorted_flagstat.txt" into flagstat
  shell:
  """
    samtools flagstat $bam > ${bam.simpleName}.sorted_flagstat.txt
  """
}


/*
* Step 8. extract with bismark
*/
process bismark_extract{
  core = 3
  cpus = core * 3
  // like in the bismark process. cf option --parallel of bismark_methylation_extractor :
  // "... will in fact use ~3 cores per value of --parallel <int> specified"
  paired_option = params.se ? '-s' : '-p --ignore_r2 2'
  tag "${bam.simpleName}"
  publishDir "${params.outdir}/bismark_extract", mode : 'symlink'
  input:
    file bam from bam_bis_dedup_2_extract
  output:
     file "${bam.baseName}.bismark.cov.gz" into bismark_methyl_cov
     file "${bam.baseName}.bedGraph.gz" into bismark_methyl_bedgraph
     file "${bam.baseName}.M-bias.txt" into bismark_methyl_bias1, bismark_methyl_bias2
     file "${bam.baseName}_splitting_report.txt" into bismark_methyl_report1, bismark_methyl_report2
     file "*${bam.baseName}.txt.gz" into bismark_methyl_info
     file "${bam.baseName}_bismark_extract.log"
  script:
  // bismark_methylation_extractor detect pe/se
    """
    echo "//SUFFIX=${bam.baseName}" && bismark_methylation_extractor --gzip --parallel ${task.core} --bedGraph --scaffolds \
    ${task.paired_option} $bam >| ${bam.baseName}_bismark_extract.log 2>&1
    """
}

/*
* Step 9. bismark report
*/
process bismarkReport {
  publishDir "${params.outdir}/bismark_report", mode : 'copy'
  input:
    file(bismark_align_log) from bismark_align_log1.collect()
    file(bismark_deduplicate_logs) from bismark_deduplicate_logs1.collect()
    file(bismark_methyl_bias) from bismark_methyl_bias1.collect()
    file(bismark_nuc_log) from bismark_nuc_log1.collect()
  output:
    file "*.html"
  script:
    """
    bismark2report
    """

}

/*
* Step 10. qualimap
*/
process qualimap {
  cpus 10
  tag "$bam.simpleName"
  publishDir "${params.outdir}/qualimap", mode: 'copy'
  input: file bam from bam_qualimap_input
  output: file "${bam.baseName}_qualimap" into qualimap
  script:
  // -p,--sequencing-protocol <arg>   Sequencing library protocol:
  //    strand-specific-forward,
  //    strand-specific-reverse or non-strand-specific (default)
  // to correct :  no --collect-overlap-pairs if se
    """
      # Avoid error: Exception in thread "main" java.awt.AWTError: Can't connect to X11 window server 
      #     using 'localhost:49.0' as the value of the DISPLAY variable.
      # cf http://qualimap.conesalab.org/doc_html/faq.html#x11problem
      export JAVA_OPTS="-Djava.awt.headless=true"
      qualimap bamqc -bam $bam -outdir ${bam.baseName}_qualimap --skip-duplicated --collect-overlap-pairs -nt ${task.cpus}
    """
}


/*
* Step 11. multiqc
*/
process multiQC {
    publishDir "${params.outdir}/", mode : 'copy'
    input:
        file('fastqc/*') from fastqc_results.collect().ifEmpty([])
        //path ('trimmomatic/*') from trimmomatic_results.collect().ifEmpty([])
        path ('mapping/*') from bismark_align_log2.collect()
        file ('qualimap/*') from qualimap.collect().ifEmpty([])
        path ('dedup/*') from bismark_deduplicate_logs2.collect()
        //path ('bismark_extract/*_splitting_report.txt') from bismark_methyl_report.collect().ifEmpty([])
        file(bismark_methyl_report_files) from  bismark_methyl_report2.collect().ifEmpty([])
        //path ('bismark_extract/*.M-bias.txt') from bismark_methyl_bias.collect().ifEmpty([])
        file(bismark_methyl_bias_files) from bismark_methyl_bias2.collect().ifEmpty([])
    output: path "multiqc_report.html"
    script:
        title = "MultiQC Report"
        """
        multiqc . --filename multiqc_report.html --title "${title}"
        """
}



/* 
* Step 12. Version of all tools used in this pipeline
*/

process software_version {
  publishDir "${params.outdir}/version", mode: 'symlink'
  output:
    file 'software_version.txt' into software_version
  script:
  """
    echo 'FastQC version:' > software_version.txt
    fastqc --version >> software_version.txt
    echo 'Trim Galore! version:' >> software_version.txt
    trim_galore --version >> software_version.txt
    echo 'Bismark version:' >> software_version.txt
    bismark --version >> software_version.txt
    echo 'Samtools version:' >> software_version.txt
    samtools --version >> software_version.txt
    """
}
